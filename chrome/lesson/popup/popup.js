var time = new Date();

var weekday = time.getDay();
var break_timer = '';
var clock, schedule = '', stat_msg = 'Loading...';

var wd_name = ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'];
var mm_name = ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
var ls_name = ['первая','вторая','третья','четвёртая','пятая','шестая','седьмая'];

var header = '<b>РАСПИСАНИЕ ЗВОНКОВ</b><br/>'+'<i>'+get_localized_date()+'</i>'+'<br><br>';

var wd_s_t = header + '1. 08:30 – 10:00 <i>| 10</i><br>2. 10:10 – 11:40 <i><b>| 30</b></i><br>3. 12:10 – 13:40 <i>| 10</i><br>4. 13:50 – 15:20 <i>| 10</i><br>5. 15:30 – 17:00 <i><b>| 20</b></i><br>6. 17:20 – 18:50 <i>| 10</i><br>7. 19:00 – 20:30 <i><u>&nbsp;&nbsp;&nbsp;&nbsp;</u></i><br>';
var sa_s_t = header + '1. 08:30 – 10:00 <i>| 10</i><br>2. 10:10 – 11:40 <i>| 10</i><br>3. 11:50 – 13:20 <i><b>| 20</b></i><br>4. 13:40 – 15:10 <i>| 10</i><br>5. 15:20 – 16:50 <i>| 10</i><br>6. 17:00 – 18:30 <i><b>| 20</b></i><br>7. 18:50 – 20:20 <i><u>&nbsp;&nbsp;&nbsp;&nbsp;</u></i><br>';

var wd_s_s = ['08:30', '10:10', '12:10', '13:50', '15:30', '17:20', '19:00'];
var wd_s_e = ['10:00', '11:40', '13:40', '15:20', '17:00', '18:50', '20:30'];
var sa_s_s = ['08:30', '10:10', '11:50', '13:40', '15:20', '17:00', '18:50'];
var sa_s_e = ['10:00', '11:40', '13:20', '15:10', '16:50', '18:30', '20:20'];

function get_localized_date() {
  return wd_name[time.getDay()] + ', ' + [time.getDate(), mm_name[time.getMonth()]].join(' ');
}

function check_lesson(data_start, data_end) {
  for (i = 0; i <= data_end.length - 1; i++) {
    if (str_to_date(clock) < str_to_date(data_start[0])) {
      stat_msg = 'Занятия ещё не начались';
    } else if (str_to_date(clock) > str_to_date(data_end[6])) {
      stat_msg = 'Занятия завершены';
    } else if ((str_to_date(clock) >= str_to_date(data_start[i])
               && (str_to_date(clock) <= str_to_date(data_end[i])))) {
      stat_msg = 'Сейчас ' + ls_name[i] + " пара" + '<br><br>';
      stat_msg += '<div>Прошло: <span>' + time_diff(clock, data_start[i]) + '</span><br>';
      stat_msg += 'Осталось: <span>' + time_diff(data_end[i], clock) + '</span><br></div>';
    } else if ((i+1 <= data_end.length-1)
               && (str_to_date(clock) >= str_to_date(data_start[i]))
               && (str_to_date(clock) <= str_to_date(data_end[i+1]))) {
      stat_msg = 'Закончилась ' + (i + 1) + '-я пара' + '<br>';
      var break_time = time_diff(data_end[i], data_start[i+1]);
      if (break_time == '00:30' || break_time == '00:20') {
        stat_msg += 'Большая перемена' + '<br>';
      } else {
        stat_msg += 'Сейчас перемена' + '<br>';
      }
      stat_msg += 'До конца перемены: ' + time_diff(data_start[i+1], clock);
    }
  }
}

function str_to_date(target) {
  return new Date(0,0,0,target.split(':')[0],target.split(':')[1]);
  alert(new Date(0,0,0,target.split(':')[0],target.split(':')[1]));
}

function time_diff(first, second) {
  var temp_output;
  if (weekday == 0) {
    temp_output = str_to_date(first) - str_to_date(second);
  } else if (weekday == 6) {
    temp_output = str_to_date(first) - str_to_date(second);
  } else {
    temp_output = str_to_date(first) - str_to_date(second);
  }
  var hours = Math.floor((temp_output % 86400000) / 3600000);
  var minutes = Math.round(((temp_output % 86400000) % 3600000) / 60000);
  return ((hours < 1) ? '' : hours + ' ч ') + ((minutes < 10) ? '0' + minutes : minutes) + ' мин';
}

function update() {
  var time = new Date();
  var weekday = time.getDay();
  var hh = (time.getHours() < 10) ? '0' + time.getHours() : time.getHours();
  var mm = (time.getMinutes() < 10) ? '0' + time.getMinutes() : time.getMinutes();
  var ss = (time.getSeconds() < 10) ? '0' + time.getSeconds() : time.getSeconds();
  clock = [hh, mm, ss].join(':');
  if (weekday == 0) {
    stat_msg = '<span>Сегодня занятий нет</span>';
  } else if (weekday == 6) {
    check_lesson(sa_s_s, sa_s_e);
    schedule = sa_s_t;
  } else {
    check_lesson(wd_s_s, wd_s_e);
    schedule = wd_s_t;
  }
  document.getElementById('time').innerHTML = clock;
  document.getElementById('less').innerHTML = stat_msg;
  document.getElementById('info').innerHTML = schedule;
  setTimeout(update, 1000);
}

update();

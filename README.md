# — Какая пара?

![License badge](https://raster.shields.io/badge/license-Do%20What%20The%20F*ck%20You%20Want%20To%20Public%20License%2C%20Version%202-lightgrey.png "License badge")

![Alt text](thumbnail.jpg?raw=true "Thumbnail")

Больше не нужно отсчитывать минуты до конца занятия.

## Демо

Увидеть как оно работает можно на [GitLab pages](https://ethicist.gitlab.io/schedule) а также [здесь](https://lesson.surge.sh/)

## Расширение для Chrome

Расширение для браузера собрано на основе первой версии.
Инструкция по установке а также исходный код и ресурсы расширения:
https://gitlab.com/Ethicist/schedule/tree/master/chrome

## Лицензия

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The F*ck You Want To Public License, Version 2,
as published by Sam Hocevar. See the [LICENSE.md](LICENSE.md) file for more details.

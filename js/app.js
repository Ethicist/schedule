var time = new Date();
var weekday = time.getDay();
var clock, schedule = '', stat_msg = '';
var temp = '', cond = '';
var wd_name = ['Воскресенье', 'Понедельник', 'Вторник',
    'Среда', 'Четверг', 'Пятница', 'Суббота'];
var mm_name = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля',
    'августа', 'сентября', 'октября', 'ноября', 'декабря'];
var ls_name = ['первая', 'вторая', 'третья', 'четвёртая', 'пятая', 'шестая', 'седьмая'];
var wd_s_s = ['08:30:00', '10:10:00', '12:10:00', '13:50:00', '15:30:00', '17:20:00', '19:00:00'];
var wd_s_e = ['10:00:00', '11:40:00', '13:40:00', '15:20:00', '17:00:00', '18:50:00', '20:30:00'];
var sa_s_s = ['08:30:00', '10:10:00', '11:50:00', '13:40:00', '15:20:00', '17:00:00', '18:50:00'];
var sa_s_e = ['10:00:00', '11:40:00', '13:20:00', '15:10:00', '16:50:00', '18:30:00', '20:20:00'];
var start = new Date(time.getFullYear(), 0, 0);
var now = Math.floor((time - start) / (1000 * 60 * 60 * 24));
var num = Math.floor(now / 2);
var BACKGROUND = document.getElementsByClassName('background')[0];
var CLOUDS_ITEM = document.getElementsByClassName('clouds')[0];
var BACKGR_URL = '';
var screen_position = 0;

// console.log('Сегодня уже ' + now + '-й день года. Шок!');

function get_localized_date() {
    return wd_name[time.getDay()] + ', ' + [time.getDate(), mm_name[time.getMonth()]].join(' ');
}var clock,time=new Date,weekday=time.getDay(),schedule="",stat_msg="",temp="",cond="",wd_name=["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],mm_name=["января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря"],ls_name=["первая","вторая","третья","четвёртая","пятая","шестая","седьмая"],wd_s_s=["08:30:00","10:10:00","12:10:00","13:50:00","15:30:00","17:20:00","19:00:00"],wd_s_e=["10:00:00","11:40:00","13:40:00","15:20:00","17:00:00","18:50:00","20:30:00"],sa_s_s=["08:30:00","10:10:00","11:50:00","13:40:00","15:20:00","17:00:00","18:50:00"],sa_s_e=["10:00:00","11:40:00","13:20:00","15:10:00","16:50:00","18:30:00","20:20:00"],start=new Date(time.getFullYear(),0,0),now=Math.floor((time-start)/864e5),num=Math.floor(now/2),BACKGROUND=document.getElementsByClassName("background")[0],CLOUDS_ITEM=document.getElementsByClassName("clouds")[0],BACKGR_URL="",screen_position=0;function get_localized_date(){return wd_name[time.getDay()]+", "+[time.getDate(),mm_name[time.getMonth()]].join(" ")}function check_lesson(e,t){for(i=0;i<=t.length-1;i++)if(str_to_date(clock)<str_to_date(e[0]))stat_msg="Занятия ещё не начались",remove_timeline();else if(str_to_date(clock)>str_to_date(t[6]))stat_msg="Занятия завершены",remove_timeline();else if(str_to_date(clock)>=str_to_date(e[i])&&str_to_date(clock)<str_to_date(t[i])){stat_msg="Сейчас "+ls_name[i]+" пара<br><br>",temp=time_diff(clock,e[i]),cond=time_diff(t[i],clock);var n=check_timediff(clock,e[i]);n/=.9,document.getElementById("timeline").style.width=n+"%"}else if(i+1<=t.length-1&&str_to_date(clock)>=str_to_date(t[i])&&str_to_date(clock)<str_to_date(e[i+1])){stat_msg="Закончилась "+(i+1)+"-я пара<br>",temp=time_diff(clock,t[i]),cond=time_diff(e[i+1],clock);var o=check_timediff(e[i+1],t[i]);o=check_timediff(clock,t[i])/(o/100),document.getElementById("timeline").style.width=o+"%",check_timediff(e[i+1],t[i])>=30?stat_msg+="Большая перемена<br><br>":stat_msg+="Сейчас перемена<br><br>",stat_msg+="Осталось "+time_diff(e[i+1],clock)}}function str_to_date(e){return new Date(0,0,0,e.split(":")[0],e.split(":")[1])}function check_timediff(e,t){var n=str_to_date(e)-str_to_date(t),o=Math.floor(n%864e5/36e5),s=Math.round(n%864e5%36e5/6e3)/10;return 0!=o&&(s+=60),s}function time_diff(e,t){var n=str_to_date(e)-str_to_date(t),o=Math.floor(n%864e5/36e5);return(o<1?"":o+" ч ")+Math.round(n%864e5%36e5/6e4)+" мин"}function check_schedule(){var e=time.getDay();0===e?document.getElementsByClassName("schedule_sat")[0].style.display="block":6===e?document.getElementsByClassName("schedule_sat")[0].style.display="block":document.getElementsByClassName("schedule_oth")[0].style.display="block"}function update(){var e=new Date,t=e.getDay(),n=e.getHours()<10?"0"+e.getHours():e.getHours(),o=e.getMinutes()<10?"0"+e.getMinutes():e.getMinutes(),s=e.getSeconds()<10?"0"+e.getSeconds():e.getSeconds(),i=[n+":"+o+'<span class="darken">'+s+"</span>"];clock=[n,o,s].join(":"),0===t?(stat_msg="<span>Сегодня занятий нет</span>",remove_timeline()):6===t?check_lesson(sa_s_s,sa_s_e):check_lesson(wd_s_s,wd_s_e),document.getElementById("time").innerHTML=i,document.getElementById("temp").innerHTML=temp,document.getElementById("cond").innerHTML=cond,document.getElementById("less").innerHTML=stat_msg,document.getElementById("date").innerHTML=get_localized_date()}function next_bg(){num>=183?num=1:num+=1,update_bg(num)}function prev_bg(){num<=1?num=365:num-=1,update_bg(num)}function rand_bg(){update_bg(num=Math.abs(Math.floor(183*Math.random())))}function update_bg(e){get_bg_link(e),BACKGROUND.style.background="url("+BACKGR_URL+")"}function add_bg_link(){BACKGR_LINK.innerHTML='<a href="'+BACKGR_URL+'" target="_blank">Открыть фото (1280x720)</a>'}function get_bg_link(e){BACKGR_URL="./bg/"+e+".jpg"}function open_bg_image(){location.replace(BACKGR_URL)}function remove_bg(){BACKGROUND.style.display="none"}function restore_bg(){update_bg(num),BACKGROUND.style.display="flex"}function remove_clouds(){CLOUDS_ITEM.style.display="none"}function restore_clouds(){CLOUDS_ITEM.style.display="block"}function remove_timeline(){document.getElementsByClassName("timeline")[0].style.display="none",document.getElementsByClassName("timeline")[1].style.display="none"}function restore_timeline(){document.getElementsByClassName("timeline")[0].style.display="block",document.getElementsByClassName("timeline")[1].style.display="block"}function slider_move(){$("#slider").carousel(screen_position),$("#slider").carousel("pause")}function dark_blur(e){0===e?BACKGROUND.style.filter="blur(0) contrast(.6) brightness(.6)":1===e?BACKGROUND.style.filter="blur(5px) contrast(.5) brightness(.5)":alert('dark_blur(state), where "state" 0 - off, 1 - on')}function toogle_blur(){document.getElementById("t_blur").checked?dark_blur(1):dark_blur(0)}function toogle_bg(){document.getElementById("t_bg").checked?restore_bg():remove_bg()}function toogle_clouds(){document.getElementById("t_clouds").checked?restore_clouds():remove_clouds()}function randomColor(){var e,t,n,o,s,i,r,c,a,l;switch(.33,i=.3685,r=(l=.55)*(1-.33*(s=6*(a=Math.random())-(o=Math.floor(6*a)))),c=l*(1-.33*(1-s)),o%6){case 0:e=l,t=c,n=i;break;case 1:e=r,t=l,n=i;break;case 2:e=i,t=l,n=c;break;case 3:e=i,t=r,n=l;break;case 4:e=c,t=i,n=l;break;case 5:e=l,t=i,n=r}return{r:Math.floor(255*e),g:Math.floor(255*t),b:Math.floor(255*n)}}function rgb2hex(e){var t,n="0123456789ABCDEF",o="#";if(e)for(t=e.split(","),i=0;i<3;i++)num=parseInt(t[i]),o+=n.charAt(num>>4)+n.charAt(15&num);return o}function random_progressbar_color(){var e=randomColor(),t=rgb2hex(e.r+","+e.g+","+e.b);document.getElementById("timeline").style.backgroundColor=t}document.getElementById("first_screen").onclick=function(){screen_position=0,slider_move()},document.getElementById("second_screen").onclick=function(){screen_position=1,slider_move()},document.getElementById("third_screen").onclick=function(){screen_position=2,slider_move()},setInterval(update,1e3),update_bg(num),dark_blur(1),check_schedule(),$(function(){$("#slider").carousel({interval:!1,keyboard:!0,ride:"carousel",pause:"hover",wrap:!1}),$(".site-wrapper").swipe({swipe:function(e,t,n,o,s,i){"right"==t&&(screen_position-=1)<0&&(screen_position=2),"left"==t&&(screen_position+=1)>2&&(screen_position=0),slider_move()},excludedElements:"label, button, input, select, textarea, a, span"})}),"serviceWorker"in navigator&&navigator.serviceWorker.register("./service-worker.js").then(function(e){}).catch(function(e){});

function check_lesson(data_start, data_end) {
    for (i = 0; i <= data_end.length - 1; i++) {
        if (str_to_date(clock) < str_to_date(data_start[0])) {
            stat_msg = 'Занятия ещё не начались';
            remove_timeline();
        } else if (str_to_date(clock) > str_to_date(data_end[6])) {
            stat_msg = 'Занятия завершены';
            remove_timeline();
        } else if ((str_to_date(clock) >= str_to_date(data_start[i])
                && (str_to_date(clock) < str_to_date(data_end[i])))) {
            stat_msg = 'Сейчас ' + ls_name[i] + " пара" + '<br><br>';
            temp = time_diff(clock, data_start[i]);
            cond = time_diff(data_end[i], clock);
            var t1 = check_timediff(clock, data_start[i]);
            t1 = t1 / 0.9;
            // FIXME: Надо обновить страницу, прогрессбар не отображается
            // restore_timeline(); не работает
            document.getElementById('timeline').style.width = t1 + '%';
        } else if (i + 1 <= data_end.length - 1) {
            if ((str_to_date(clock) >= str_to_date(data_end[i])) 
                && (str_to_date(clock) < str_to_date(data_start[i + 1]))) {
                stat_msg = 'Закончилась ' + (i + 1) + '-я пара' + '<br>';
                temp = time_diff(clock, data_end[i]);
                cond = time_diff(data_start[i + 1], clock);
                var t2 = check_timediff(data_start[i+1], data_end[i]); // всё время перемены
                t2 = (check_timediff(clock, data_end[i])) / (t2/100);
                document.getElementById('timeline').style.width = t2+'%';
                if ((check_timediff(data_start[i + 1], data_end[i])) >= 30) {
                    stat_msg += 'Большая перемена' + '<br><br>';
                } else {
                    stat_msg += 'Сейчас перемена' + '<br><br>';
                }
                stat_msg += 'Осталось ' + time_diff(data_start[i + 1], clock);
            }
        }
    }
}

function str_to_date(target) {
    return new Date(0, 0, 0, target.split(':')[0], target.split(':')[1]);
    alert(new Date(0, 0, 0, target.split(':')[0], target.split(':')[1]));
}

function check_timediff(first, second) {
    var temp_output = str_to_date(first) - str_to_date(second);
    var hours = Math.floor((temp_output % 86400000) / 3600000);
    var output = Math.round(((temp_output % 86400000) % 3600000) / 6000) / 10;
    if (hours != 0) {
        output += 60;
    }
    return output;
}

function time_diff(first, second) {
    var temp_output = str_to_date(first) - str_to_date(second);
    var hours = Math.floor((temp_output % 86400000) / 3600000);
    var minutes = Math.round(((temp_output % 86400000) % 3600000) / 60000);
    return ((hours < 1) ? '' : hours + ' ч ') + minutes + ' мин';
}

function check_schedule() {
    var weekday = time.getDay();
    if (weekday === 0) {
        document.getElementsByClassName('schedule_sat')[0].style.display = "block";
    } else if (weekday === 6) {
        document.getElementsByClassName('schedule_sat')[0].style.display = "block";
    } else {
        document.getElementsByClassName('schedule_oth')[0].style.display = "block";
    }
}

function update() {
	var t_time = new Date();
    var weekday = t_time.getDay();
    var hh = (t_time.getHours() < 10) ? '0' + t_time.getHours() : t_time.getHours();
    var mm = (t_time.getMinutes() < 10) ? '0' + t_time.getMinutes() : t_time.getMinutes();
    var ss = (t_time.getSeconds() < 10) ? '0' + t_time.getSeconds() : t_time.getSeconds();
    var out = [hh + ":" + mm + '<span class="darken">' + ss + '</span>'];
    clock = [hh, mm, ss].join(':');
    if (weekday === 0) {
        stat_msg = '<span>Сегодня занятий нет</span>';
        remove_timeline();
    } else if (weekday === 6) {
        check_lesson(sa_s_s, sa_s_e);
    } else {
        check_lesson(wd_s_s, wd_s_e);
    }
    document.getElementById('time').innerHTML = out;
    document.getElementById('temp').innerHTML = temp;
    document.getElementById('cond').innerHTML = cond;
    document.getElementById('less').innerHTML = stat_msg;
    document.getElementById('date').innerHTML = get_localized_date();
}

/* * * * * * *
 * Interface
 */

// Предыдущий фон
function next_bg() {
    if (num >= 183) {
        num = 1;
    } else {
        num = num + 1;
    }
    update_bg(num);
}

// Следующий фон
function prev_bg() {
    if (num <= 1) {
        num = 365;
    } else {
        num = num - 1;
    }
    update_bg(num);
}

// Случайный фон
function rand_bg() {
    num = Math.abs(Math.floor(Math.random() * 183));
    update_bg(num);
}

// Установить или обновить фоновое изображение
function update_bg(index) {
    get_bg_link(index);
    BACKGROUND.style.background = ('url(' + BACKGR_URL + ')');
}

// Добавить ссылку на фоновую картинку
function add_bg_link() {
    BACKGR_LINK.innerHTML = '<a href=\"' + BACKGR_URL + '\" target="_blank">Открыть фото (1280x720)</a>';
}

// Получить ссылку на фоновое изображение
function get_bg_link(num) {
    BACKGR_URL = './bg/' + num + '.jpg';
}

// Открыть или загрузить изображение
function open_bg_image() {
    location.replace(BACKGR_URL);
}

// Убрать фон
function remove_bg() {
    BACKGROUND.style.display = 'none';
}

// Вернуть фон
function restore_bg() {
    update_bg(num);
    BACKGROUND.style.display = 'flex';
}

// Убрать облака
function remove_clouds() {
    CLOUDS_ITEM.style.display = 'none';
}

// Вернуть облака
function restore_clouds() {
    CLOUDS_ITEM.style.display = 'block';
}

// Скрыть или показать прогрессбар
function remove_timeline() {
    document.getElementsByClassName('timeline')[0].style.display = 'none';
    document.getElementsByClassName('timeline')[1].style.display = 'none';
}

// Не работает !!!
function restore_timeline() {
    document.getElementsByClassName('timeline')[0].style.display = 'block';
    document.getElementsByClassName('timeline')[1].style.display = 'block';
}

function slider_move() {
    $('#slider').carousel(screen_position);
    $('#slider').carousel('pause');
}

/* * 
 * Эффект плавного затемнения фонового изображения при наведении
 * курсора на первый экран с часами и информацие о текущей паре.
 * Кроме того, имеются кнопки-слайдеры на экране с настройками.
 * UPD: Из-за вырвиглазно ярких фонов включен по умолчанию.
 */
function dark_blur(state) {
    if (state === 0) {
        BACKGROUND.style.filter = 'blur(0) contrast(.6) brightness(.6)';
        //CLOUDS_ITEM.style.filter = 'blur(0) contrast(.6) brightness(.6)';
    } else if (state === 1) {
        BACKGROUND.style.filter = 'blur(5px) contrast(.5) brightness(.5)';
        //CLOUDS_ITEM.style.filter = 'blur(5px) contrast(.5) brightness(.5)';
    } else {
        alert('dark_blur(state), where "state" 0 - off, 1 - on');
    }
}

/*
 
 // Наведение курсора, или тап по центру экрана, в области с часами
 
 document.getElementsByClassName('cover')[0].onmouseover = function() {
 dark_blur(1);
 };
 document.getElementsByClassName('cover')[0].onmouseout = function() {
 dark_blur(0);
 };
 
 */

// Включение-выключение свистоперделок на checkbox в настройках

function toogle_blur() {
    var target = document.getElementById('t_blur');
    if (target.checked) {
        dark_blur(1);
    } else {
        dark_blur(0);
    }
}
function toogle_bg() {
    var target = document.getElementById('t_bg');
    if (target.checked) {
        restore_bg();
    } else {
        remove_bg();
    }
}
function toogle_clouds() {
    var target = document.getElementById('t_clouds');
    if (target.checked) {
        restore_clouds();
    } else {
        remove_clouds();
    }
}

function randomColor() {
  var r, g, b, i, f, p, q, t, h, s, v;
  h = Math.random();
  s = 0.33;
  v = 0.55;
  i = Math.floor(h * 6);
  f = h * 6 - i;
  p = v * (1 - s);
  q = v * (1 - f * s);
  t = v * (1 - (1 - f) * s);
  switch (i % 6) {
    case 0: r = v, g = t, b = p; break;
    case 1: r = q, g = v, b = p; break;
    case 2: r = p, g = v, b = t; break;
    case 3: r = p, g = q, b = v; break;
    case 4: r = t, g = p, b = v; break;
    case 5: r = v, g = p, b = q; break;
  } return {
	  r: Math.floor(r * 255), g: Math.floor(g * 255), b: Math.floor(b * 255)
  };
}

function rgb2hex(input) {
  var x = '0123456789ABCDEF';
  var input, component, output = '#';
  if (input) {
    component = input.split(',');
    for (i = 0; i < 3; i++) {
      num = parseInt(component[i]);
      output += x.charAt(num >> 4) + x.charAt(num & 15)
    }
  }
  return output;
}

function random_progressbar_color() {
	var color = randomColor();
    var out = rgb2hex(color.r+","+color.g+","+color.b);
	document.getElementById('timeline').style.backgroundColor = out;
}

/* *
 * Горизонтальная навигация по ссылкам в шапке страницы,
 * переключение слайдера.
 */
document.getElementById('first_screen').onclick = function() {
	screen_position = 0;
    slider_move();
};
document.getElementById('second_screen').onclick = function() {
	screen_position = 1;
    slider_move();
};
document.getElementById('third_screen').onclick = function() {
	screen_position = 2;
    slider_move();
};

setInterval(update, 1000);
update_bg(num);
dark_blur(1);
check_schedule();
//remove_clouds();
//random_progressbar_color();


$(function() {
    $('#slider').carousel({
        interval: false,
        keyboard: true,
        ride: 'carousel',
        pause: 'hover',
        wrap: false
    });
	$(".site-wrapper").swipe({
	  swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
		if (direction == 'right') {
		    screen_position -= 1;
			if (screen_position < 0) {
				screen_position = 2;
			}
		}
		if (direction == 'left'){
			screen_position += 1;
			if (screen_position > 2) {
				screen_position = 0;
			}
		}
		slider_move();
	  },
	  excludedElements:"label, button, input, select, textarea, a, span"
	});
});

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('./service-worker.js').then(function(reg){
  }).catch(function(err) {
  });
}
